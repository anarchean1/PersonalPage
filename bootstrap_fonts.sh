#!/bin/bash

link_ver=4_1
font_ver=4106

if [ ! -f FiraFonts.zip ]; then
    wget -O FiraFonts.zip http://www.carrois.com/downloads/fira_$link_ver/FiraFonts$font_ver.zip
fi

unzip FiraFonts.zip
mkdir -p fonts/FiraSans

for dir in OTF WEB/EOT WEB/TTF WEB/WOFF WEB/WOFF2; do
    mv FiraFonts$font_ver/$dir/* fonts/FiraSans/
done

